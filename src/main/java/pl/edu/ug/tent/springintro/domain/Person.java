package pl.edu.ug.tent.springintro.domain;

public class Person {

  private String first_name;
  private String last_name;
  private String email;
  private String company_name;

  public Person() {
    System.out.println("Creating person " + this);
  }

  public Person(String first_name, String last_name, String email, String company_name) {
    this.first_name = first_name;
    this.last_name = last_name;
    this.email = email;
    this.company_name = company_name;
    System.out.println("Creating person " + this);
  }

  public String getName() {
    return first_name;
  }

  public void setName(String name) {
    this.first_name = name;
  }

  @Override
  public String toString() {
    return "Person{" +
        "first name='" + first_name + '\'' +
        ", last name=" + last_name + '\'' +
        ", email=" + email + '\'' +
        ", company name=" + company_name +
        '}';
  }
}
