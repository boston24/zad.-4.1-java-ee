package pl.edu.ug.tent.springintro;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ImportResource;
import pl.edu.ug.tent.springintro.domain.Person;

import java.util.Arrays;
import java.util.Map;

@SpringBootApplication
@ImportResource("classpath:beans.xml")
public class SpringintroApplication {

	public static void main(String[] args) {

		ApplicationContext context = SpringApplication.run(SpringintroApplication.class, args);

		Person person1 = (Person) context.getBean("prezes");

		System.out.println(person1.toString());

		Person person2 = (Person) context.getBean("wiceprezes");

		System.out.println(person2.toString());

		Person person3 = (Person) context.getBean("sekretarka");

		System.out.println(person3.toString());

		Map<String, Person> map = context.getBeansOfType(Person.class);
		map.forEach((key,value)->System.out.println(value));

	}

}




